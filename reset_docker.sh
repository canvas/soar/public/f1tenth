#!/bin/bash

# container name is same as folder name
CONTAINER_NAME="f1tenth"


# Check if the container already exists
if [ "$(docker ps -a -q -f name="$CONTAINER_NAME")" ]; then
    # Check if the container is running
    if [ "$(docker ps -q -f name="$CONTAINER_NAME")" ]; then
        echo -e "Stopping running container..."
        docker stop "$CONTAINER_NAME" >/dev/null 2>&1
    fi

    echo -e "Removing container..."
    docker rm "$CONTAINER_NAME" >/dev/null 2>&1
fi

# if [ -f ./run_docker.sh ]; then
#     echo -e "Running run_docker.sh script..."
#     ./run_docker.sh
# else
#     echo -e "run_docker.sh script not found in current directory"
# fi
