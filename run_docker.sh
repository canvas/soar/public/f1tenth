#!/bin/bash

# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

architecture=$(uname -m)
if [[ $architecture == *"arm"* ]]; then
    arch="arm"
elif [[ $architecture == *"x86"* ]]; then
    arch="amd"
else
    echo "${RED}This is an unknown architecture.${NC}"
    exit 1;
fi

# --- USER CONFIG --- #
CONTAINER_NAME="f1tenth"
# ------------------- #


NETWORK_NAME="ros-net"

# user info
USER_ID=$(id -u)
GROUP_ID=$(id -g)

# Check if Docker is running
docker info >/dev/null 2>&1 || { echo "Error: Docker is not running"; exit 1; }

# Function to handle exit signal
on_exit() {
    echo -e "${RED}Exiting the container...${NC}"
    docker stop "$CONTAINER_NAME" >/dev/null 2>&1 & # stops all instances connected to the container
}

trap on_exit EXIT

# Check if the Docker network exists, only if NETWORK_NAME is set
if [[ -n "$NETWORK_NAME" ]]; then
    if ! docker network ls | grep -q "$NETWORK_NAME"; then
        docker network create "$NETWORK_NAME"
    fi
    echo -e "Using network: ${BLUE}$NETWORK_NAME${NC}"
else
    echo -e "Using ${BLUE}default Docker network${NC}"
fi


###### ***** RUN IMAGE IF ALREADY BUILT ***** #####
if [ "$(docker ps -a -q -f name="$CONTAINER_NAME")" ]; then
    # Check if the container is running
    if [ "$(docker ps -q -f name="$CONTAINER_NAME")" ]; then
        echo -e "Container is ${GREEN}already running${NC}"
    else
        echo -e "Container exists but is not running, starting container..."
        docker start "$CONTAINER_NAME" >/dev/null 2>&1
    fi

    echo -e "${GREEN}Connecting to container ${BLUE}${CONTAINER_NAME}${NC}"

    # Check if a TTY is available and run the appropriate command
    if [ -t 1 ]; then
        docker exec -it "$CONTAINER_NAME" bash
    else
        docker exec "$CONTAINER_NAME" bash -c "echo 'Non-interactive mode'"
    fi

else
    ##### ***** BUILD IMAGE IF IMAGE NOT FOUND ***** #####
    # If the container does not exist, build the image and run the container
    echo -e "${GREEN}Building the Docker image for ${architecture} architecture...${NC}"

    # delete any existing Dockerfile
    rm -f Dockerfile

    # build dynamic Dockerfile
    if [ $arch = "arm" ]; then
        echo "FROM arm64v8/ros:iron" > Dockerfile
    else
        echo "FROM osrf/ros:iron-desktop" > Dockerfile
    fi

    # create a temporary Dockerfile that includes Dockerfile.template
    cat docker/Dockerfile.template >> Dockerfile

    ##### BUILD IMAGE #####
    docker build \
        --rm \
        --build-arg USERNAME=$USER \
        --build-arg USERID=$USER_ID \
        --build-arg USERGROUP=$GROUP_ID \
        --network host \
        --build-arg ARCH=${arch} \
        -t ros2:latest \
        .

    rm -f Dockerfile

    echo -e "${GREEN}Connecting to container ${BLUE}${CONTAINER_NAME}${NC}"

    # check for tty
    tty_arg=""
    if [ ! -t 1 ]; then
        tty_arg="-d"
    fi

    # check for network
    network_arg=""
    if [[ -n "$NETWORK_NAME" ]]; then
        network_arg="--network ${NETWORK_NAME}"
    fi


    additional_args=""

    if [ -n "$XSOCK" ]; then
        additional_args+="--volume=$XSOCK:$XSOCK:rw "
    fi

    if [ -n "$XAUTH" ]; then
        additional_args+="--volume=$XAUTH:$XAUTH:rw "
    fi

    if [ -n "$SSH_DIR" ]; then
        additional_args+="--volume=$SSH_DIR:$SSH_DIR:Z "
    fi

    if [ -n "$GIT_CONFIG" ]; then
        additional_args+="--volume=$GIT_CONFIG:$GIT_CONFIG:rw "
    fi

    # Always add this volume
    additional_args+="--volume=$(pwd)/src:/ros2_ws/src:rw -p 8765:8765"


    ##### RUN BUILT IMAGE #####
    docker run \
        $network_arg \
        $tty_arg \
        -it \
        -u $USER_ID:$GROUP_ID \
        $additional_args \
        --name "$CONTAINER_NAME" \
        ros2:latest
fi
